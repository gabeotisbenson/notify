import a11yColor from 'a11ycolor';
import color from 'color';

export default bgColor => a11yColor(color(bgColor).isLight(bgColor) ? '#000' : '#fff', bgColor, 'small');

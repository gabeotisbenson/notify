/* global Notification */
// const checkForPermission = (callback) => {
// 	if (Notification.permission === 'granted') {
// 		callback();
// 	} else if (Notification.permission === 'denied') {
// 		callback(new Error('Notifications are disallowed by the user'));
// 	} else {
// 		Notification.requestPermission(() => {
// 			checkForPermission(callback);
// 		});
// 	}
// };

const supportsNotifications = () => {
	return 'Notification' in window && Notification.permission === 'granted';
};

export default supportsNotifications;

export const getElementFromHTML = html => {
	const div = document.createElement('div');
	div.innerHTML = html;
	return div.firstChild;
};

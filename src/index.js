import defaultOptions from '~/default-options';
// import getAccessibleFgColor from '~/utilities/get-accessible-fg-color';
import messageContainer from '~/pug/message-container';
import Timer from '~/utilities/timer';
import tray from '~/pug/tray';
import '~/style/index.styl';
import { getElementFromHTML } from '~/utilities/get-element-from-html';

let options = defaultOptions;

const fgPostFix = 'Bg';

const animationTime = 400;
const timeout = 5000;

const configure = newOptions => {
	newOptions.colors = Object.assign(options.colors, newOptions.colors);
	// for (let key in options.colors) options.colors[`${key}${fgPostFix}`] = getAccessibleFgColor(options.colors[key]);
	for (let key in options.colors) options.colors[`${key}${fgPostFix}`] = '#000';
	newOptions.headings = Object.assign(options.headings, newOptions.headings);
	options = Object.assign(options, newOptions);
};

const shouldAutoHide = status => options.autoHide[status] === true;

const createNotificationTray = () => document.querySelector('body').appendChild(getElementFromHTML(tray()));

const constructMessageContainer = (status, message, messageHeading) => {
	// Create a message container div
	const container = getElementFromHTML(messageContainer({
		colors: {
			background: options.colors[status],
			foreground: options.colors[`${status}${fgPostFix}`]
		},
		message: {
			classes: options.squareCorners === true ? `${status} square-corners` : status,
			heading: messageHeading || options.headings[status],
			text: message
		},
		svgSize: 10
	}));

	// Attach close function to the container's handle
	const handle = container.querySelector('button.handle');
	handle.addEventListener('click', () => {
		container.classList.add('hide');
		container.style.transform = `translateY(-${container.offsetHeight}px)`;
		setTimeout(() => {
			if (container.parentNode) container.parentNode.removeChild(container);
		}, animationTime);
	});

	// Finally, attach the fade in function to the element
	container.fadeIn = () => {
		const tray = document.querySelector('div.notify-tray');
		tray.insertBefore(container, tray.children[0]);
		container.classList.remove('hide');
	};

	return container;
};

const attachFadeOut = el => {
	const timer = new Timer(() => {
		el.classList.add('hide');
		el.style.transform = `translateY(-${el.offsetHeight}px)`;
		setTimeout(() => {
			if (el.parentNode) el.parentNode.removeChild(el);
		}, animationTime);
	}, timeout);

	el.addEventListener('mouseover', () => {
		timer.pause();
	});
	el.addEventListener('mouseleave', () => {
		timer.resume();
	});
};

const sendNotification = (status, message, heading) => {
	if (!document.querySelector('div.notify-tray')) createNotificationTray();
	const messageContainer = constructMessageContainer(status, message, heading);
	messageContainer.fadeIn();
	if (shouldAutoHide(status)) attachFadeOut(messageContainer);

	return messageContainer;
};

const error = (message, heading = false) => sendNotification('error', message);
const log = (message, heading = false) => sendNotification('log', message);
const warn = (message, heading = false) => sendNotification('warn', message);
const clear = () => {
	if (!document.querySelector('div.notify-tray')) createNotificationTray();
	const messageContainers = document.querySelectorAll('div.message-container');
	// fuck IE
	Array.from(messageContainers).forEach(el => {
		el.parentNode.removeChild(el);
	});
};

export default {
	clear,
	configure,
	error,
	log,
	warn
};

import config from './rollup.config.shared.js';

export default {
	...config,
	input: './src/index.js'
};

import alias from 'rollup-plugin-alias';
import babel from 'rollup-plugin-babel';
import builtins from 'rollup-plugin-node-builtins';
import commonjs from 'rollup-plugin-commonjs';
import del from 'rollup-plugin-delete';
import json from 'rollup-plugin-json';
import path from 'path';
import pkg from './package.json';
import postcss from 'rollup-plugin-postcss';
import pug from 'rollup-plugin-pug';
import resolve from 'rollup-plugin-node-resolve';
import visualizer from 'rollup-plugin-visualizer';
import { eslint } from 'rollup-plugin-eslint';
import { terser } from 'rollup-plugin-terser';

const fileTypes = ['.js', '.json', '.styl', '.pug'];

export default {
	input: 'src/index.js',
	output: {
		name: 'notify',
		file: 'dist/notify.js',
		format: 'umd',
		sourcemap: true
	},
	plugins: [
		del({ targets: ['dist/*', 'stats.html'] }),
		builtins(),
		commonjs(),
		json({
			preferConst: true,
			compact: true
		}),
		alias({
			resolve: fileTypes,
			entries: [
				{ find: /^~\/(.*)/, replacement: path.resolve(__dirname, 'src', '$1') }
			]
		}),
		resolve({
			browser: true,
			extensions: fileTypes
		}),
		pug({ pugRuntime: 'pug-runtime' }),
		eslint({ include: ['src/**/*.js'] }),
		postcss({
			extensions: ['.styl']
			// extract: true
		}),
		babel(),
		terser(),
		visualizer({
			filename: './STATS.html',
			sourcemap: true,
			title: `${pkg.name} Bundle Size Statistics`
		})
	]
};

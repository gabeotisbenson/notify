# Notify.js
Notify.js is a simple library for displaying notifications on an HTML page using
JavaScript.

It is meant to be analogous to the JavaScript `console` object, providing
helpful `.log()`, `.warn()`, and `.error()` methods.  There is also a `.clear()`
method, which will simply clear all notifications from the tray.

[Check out this demo](https://gabe.xyz/notify) to play around with it and to
test various configurations on the fly!

## Installation
You have two options for using this library.  You may simply download the
library, copy notify.js to your project directory, and load the script on your
page to be able to use its functions.

Alternatively you may install the library using `npm i @gabeotisbenson/notify`,
and then use `import notify from '@gabeotisbenson/notify'` to import the library
into your project.

## Usage
It's simple! Just load the script on the page, and then use `notify` the same
way you'd use `console`!  Note that you may pass HTML into any of the functions
and that HTML will be rendered within the notification.
```html
<script type="text/javascript" src="notify.js"></script>
<script type="text/javascript">
	notify.log('This is a regular notification!');
	notify.warn('Yeesh, be careful!');
	notify.error('<strong>Uh-oh</strong>, something broke :-(');
</script>
```

You may also configure notify (defaults specified here):
```js
notify.configure({
  'squareCorners': false,
  'autoHide': {
    'error': false,
    'log': true,
    'warn': true
  },
  'colors': {
    'error': '#EC5f67',
    'log': '#99C794',
    'warn': '#FAC863'
  },
  'headings': {
    'log': '',
    'error': 'Error:',
    'warn': 'Warning:'
  }
});
```

## Credit
Thanks to Tim Down and Robert for their handy Timer function detailed here:
https://stackoverflow.com/questions/3969475/javascript-pause-settimeout

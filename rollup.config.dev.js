import config from './rollup.config.shared.js';
import html from 'rollup-plugin-bundle-html';
import serve from 'rollup-plugin-serve';

export default {
	...config,
	input: './src/index.js',
	plugins: [
		...config.plugins,
		html({
			template: 'src/index.html',
			dest: 'dist/'
		}),
		serve({
			contentBase: 'dist',
			port: 1234
		})
	]
};
